const MIN_DURATION = 200
const MAX_DURATION = 1000

const random = max => parseInt(Math.random() * max)

function initShape (shape) {
  shape.style.transition = 'opacity 0.3s ease-in-out'
  shape.style.opacity = 0
}

function randomlyShowShape (shape) {
  const time = MIN_DURATION + random(MAX_DURATION - MIN_DURATION)
  setTimeout(() => {
    shape.style.opacity = 1
  }, time)
}

export default function ({ svg, selector }) {
  const shapes = svg.querySelectorAll(selector)
  shapes.forEach(initShape)
  shapes.forEach(randomlyShowShape)
}
