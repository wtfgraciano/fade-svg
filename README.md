fade-svg
========

Function to randomly fade elements from a given selector of an svg

## Usage

```javascript
import fadeSVG from 'fade-svg'

const mySvg = document.querySelector('svg')

fadeSVG({ svg: svg, selector: 'polygon' }) // will randomly fade-in all polygons inside the svg
```
**Note:** your svg MUST be inline. You can't have a `img` tag with an svg source.

Just `npm install fade-svg` and then you can import it in your code.

