# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## V0.1.1

### Added
- README.md
- LICENSE

### Changed
- Changed keywords and description in package.json that wrongly said it was a web component (it was the original idea)

## v0.1.0

### Added
- Fisrt implementation, using the contract `fadeSVG({ svg, selector })`
- Unit tests & eslint
- Working example, with `index.html` and `example.js`
- This changelog file