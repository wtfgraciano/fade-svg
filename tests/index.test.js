import fadeSVG from '../index.js'

jest.useFakeTimers()

const originalWindow = { ...window }
const windowSpy = jest.spyOn(global, 'window', 'get')

const mockSVGShape = { style: { opacity: 1 } }
const mockSVGShapes = [
  { ...mockSVGShape },
  { ...mockSVGShape },
  { ...mockSVGShape },
  { ...mockSVGShape }
]
const mockSVGQuerySelectorAll = jest.fn(() => mockSVGShapes)
const mockSVG = {
  querySelectorAll: mockSVGQuerySelectorAll
}
const mockSelector = 'some-selector'

describe('fadeSVG', () => {
  it('is a function', () => {
    expect(typeof fadeSVG).toEqual('function')
  })
  describe('when passing an svg element and a selector', () => {
    beforeEach(() => {
      jest.useFakeTimers()
      windowSpy.mockImplementation(() => ({
        ...originalWindow
      }))
      fadeSVG({ svg: mockSVG, selector: mockSelector })
    })

    afterEach(() => {
      windowSpy.mockReset()
    })

    it('fades one part of the svg at least once', () => {
      expect(mockSVGShapes[0].style.opacity).toEqual(0)
    })

    it('shows some of the parts when a time has passed', () => {
      jest.advanceTimersByTime(500)
      const shapesShowing = mockSVGShapes
        .filter(({ style: { opacity } }) => opacity === 1)
      expect(shapesShowing.length).toBeGreaterThan(0)
    })
    it('shows ALL parts when the duration has passed', () => {
      jest.advanceTimersByTime(1000)
      const zeroOpacities = mockSVGShapes
        .filter(({ style: { opacity } }) => opacity === 0)
      expect(zeroOpacities.length).toEqual(0)
    })
  })
})
